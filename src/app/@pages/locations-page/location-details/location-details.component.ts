import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LocationsService } from '../../../shared/services/locations.service';

@Component({
  selector: 'app-location-details',
  templateUrl: './location-details.component.html',
  styleUrls: ['./location-details.component.scss']
})
export class LocationDetailsComponent implements OnInit {

  location?:any=[];


  constructor(private route:ActivatedRoute, private locationsService:LocationsService) { }

  ngOnInit(): void {

    this.route.paramMap.subscribe(params=>{
      const idLocation:any= params.get('id');
      this.locationsService.getLocationsByID(idLocation).subscribe((location)=>{
        this.location=location;
        console.log(this.location);
      })
    })

  }

}
