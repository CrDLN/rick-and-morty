import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LocationsPageComponent } from './locations-page.component';
import { LocationDetailsComponent } from './location-details/location-details.component';

const routes: Routes = [
  {path:'',component:LocationsPageComponent},
  {path:':id',component:LocationDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocationsPageRoutingModule { }
