import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LocationsPageRoutingModule } from './locations-page-routing.module';
import { LocationsPageComponent } from './locations-page.component';
import { LocationDetailsComponent } from './location-details/location-details.component';
import { LocationsGalleryComponent } from './locations-gallery/locations-gallery.component';


@NgModule({
  declarations: [
    LocationsPageComponent,
    LocationDetailsComponent,
    LocationsGalleryComponent
  ],
  imports: [
    CommonModule,
    LocationsPageRoutingModule
  ]
})
export class LocationsPageModule { }
