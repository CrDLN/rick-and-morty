import { Component, OnInit } from '@angular/core';
import { LocationsService } from '../../shared/services/locations.service';

@Component({
  selector: 'app-locations-page',
  templateUrl: './locations-page.component.html',
  styleUrls: ['./locations-page.component.scss']
})
export class LocationsPageComponent implements OnInit {


  locations:any=[];
  info:any={}

  constructor(private locationsService:LocationsService) { }

  ngOnInit(): void {
    this.locationsService.getLocations().subscribe((locations:any)=>{
      this.locations=locations.results;
      this.info=locations.info;
      console.log(this.locations);
    })
  }

}
