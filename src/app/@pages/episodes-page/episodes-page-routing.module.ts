import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EpisodeDetailsComponent } from './episode-details/episode-details.component';
import { EpisodesPageComponent } from './episodes-page.component';

const routes: Routes = [
  {path:'', component:EpisodesPageComponent},
  {path:':id', component:EpisodeDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EpisodesPageRoutingModule { }
