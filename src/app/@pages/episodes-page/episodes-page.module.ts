import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EpisodesPageRoutingModule } from './episodes-page-routing.module';
import { EpisodesPageComponent } from './episodes-page.component';
import { EpisodeDetailsComponent } from './episode-details/episode-details.component';
import { EpisodesGalleryComponent } from './episodes-gallery/episodes-gallery.component';


@NgModule({
  declarations: [
    EpisodesPageComponent,
    EpisodeDetailsComponent,
    EpisodesGalleryComponent
  ],
  imports: [
    CommonModule,
    EpisodesPageRoutingModule
  ]
})
export class EpisodesPageModule { }
