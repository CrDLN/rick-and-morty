import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EpisodesService } from '../../../shared/services/episodes.service';

@Component({
  selector: 'app-episode-details',
  templateUrl: './episode-details.component.html',
  styleUrls: ['./episode-details.component.scss']
})
export class EpisodeDetailsComponent implements OnInit {

  episode?:any={}

  constructor(private episodesService:EpisodesService, private route:ActivatedRoute) { }

  ngOnInit(): void {

    this.route.paramMap.subscribe(params =>{
      const idEpisode = params.get('id');
      this.episodesService.getEpisodesByID(idEpisode).subscribe((episode)=>{
        this.episode=episode;
        console.log(this.episode)
      })
    })

  }

}
