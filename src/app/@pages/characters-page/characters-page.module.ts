import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CharactersPageRoutingModule } from './characters-page-routing.module';
import { CharactersPageComponent } from './characters-page.component';
import { CharacterDetailsComponent } from './character-details/character-details.component';
import { CharactersGalleryComponent } from './characters-gallery/characters-gallery.component';


@NgModule({
  declarations: [
    CharactersPageComponent,
    CharacterDetailsComponent,
    CharactersGalleryComponent
  ],
  imports: [
    CommonModule,
    CharactersPageRoutingModule
  ]
})
export class CharactersPageModule { }
