import { Component, OnInit } from '@angular/core';
import { CharactersService } from '../../shared/services/characters.service';

@Component({
  selector: 'app-characters-page',
  templateUrl: './characters-page.component.html',
  styleUrls: ['./characters-page.component.scss']
})
export class CharactersPageComponent implements OnInit {

  characters:any=[]
  info:any={}

  constructor(private charactersService:CharactersService) { }

  ngOnInit(): void {

    this.charactersService.getCharacters().subscribe((characters:any)=>{
      this.characters=characters.results;
      this.info=characters.info;
      console.log(this.characters)
    })
  }

}
