import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CharactersService } from '../../../shared/services/characters.service';

@Component({
  selector: 'app-character-details',
  templateUrl: './character-details.component.html',
  styleUrls: ['./character-details.component.scss']
})
export class CharacterDetailsComponent implements OnInit {

  //Inicializamos para que no salte error undefined
  character:any={
    image:'',
    name:'',
    id:0,
    species:'',
    status:'',
    origin:{},
    location:{}
  };

  constructor(private charactersService:CharactersService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params)=>{
      const idCharacter = params.get('id');
      this.charactersService.getCharactersByID(idCharacter).subscribe((character:any)=>{
        this.character = character;
        console.log(this.character)
      })
    })
  }

}
