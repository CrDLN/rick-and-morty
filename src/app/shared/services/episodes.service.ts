import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EpisodesService {

  constructor(private httpClient:HttpClient) { }

  getEpisodes(){
    return this.httpClient.get('https://rickandmortyapi.com/api/episode');
  }

  getEpisodesByID(episodeID:any){
    return this.httpClient.get('https://rickandmortyapi.com/api/episode/' + episodeID);
  }
  

}
