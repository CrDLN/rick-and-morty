import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CharactersService {

  constructor(private httpClient:HttpClient) { }

  getCharacters(){
    return this.httpClient.get('https://rickandmortyapi.com/api/character');
  }

  getCharactersByID(characterID:any){
    return this.httpClient.get('https://rickandmortyapi.com/api/character/' + characterID);
  }

}
